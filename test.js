const {typeDefs} = require("graphql-scalars");
const {makeExecutableSchema} = require("graphql-tools");

const schema = makeExecutableSchema({typeDefs});
console.log(schema);

module.exports = schema;