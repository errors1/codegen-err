// Just for the purpose of providing root type Query
import gql from "graphql-tag";

export const gqlTypeDefs = gql`
  type Query{
    user: User!
  }

  type User {
    id: ID
    name: EmailAddress!
    email: String!
  } 
`;
